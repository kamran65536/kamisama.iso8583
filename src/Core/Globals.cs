﻿global using KamiSama.Extensions;
global using System.Diagnostics.CodeAnalysis;

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("KamiSama.Iso8583.Core.Tests")]