﻿using System.Text;

namespace KamiSama.Iso8583;

/// <summary>
/// String Iso Part, contains human readable string data from the iso 8583 message
/// </summary>
public class TextIsoPart : IsoPart
{
	/// <summary>
	/// inner strign data
	/// </summary>
	public string Data { get; set; }
	/// <summary>
	/// constrcutor
	/// </summary>
	/// <param name="data">input data</param>
	public TextIsoPart(string data) => Data = data;
	/// <summary>
	/// constuctor
	/// </summary>
	public TextIsoPart() => Data = string.Empty;
	/// <summary>
	/// returns <see cref="Data"/>
	/// </summary>
	/// <returns></returns>
	public override string ToString() => this.Data;
	/// <summary>
	/// returns <see cref="UTF8Encoding"/> equivalant bytes
	/// </summary>
	/// <returns></returns>
	public override ReadOnlyMemory<byte> ToBytes() => Encoding.UTF8.GetBytes(Data);
}
