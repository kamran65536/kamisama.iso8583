﻿namespace KamiSama.Iso8583;

/// <summary>
/// Bitmap Iso Part, usually represents P00 (Primary bitmap) or P01 (Secondary Bitmap) data from iso message
/// </summary>
public class BitmapIsoPart : IsoPart
{
	/// <summary>
	/// inner flags
	/// </summary>
	internal readonly bool[] flags = new bool[64];
	/// <summary>
	/// returns field's flag
	/// </summary>
	/// <param name="fieldNo">the field number to return</param>
	/// <returns></returns>
	public bool this[int fieldNo]
	{
		get
		{
			int org = fieldNo;
			if (fieldNo > 64)
				fieldNo -= 64;

			if ((fieldNo <= 0) || (fieldNo > 64))
				throw new IndexOutOfRangeException($"Invalid field no: {org}");

			return flags[fieldNo - 1];
		}
		set
		{
			var org = fieldNo;
			if (fieldNo > 64)
				fieldNo -= 64;

			if ((fieldNo <= 0) || (fieldNo > 64))
				throw new IndexOutOfRangeException($"Invalid field no: {org}");

			flags[fieldNo - 1] = value;
		}
	}
	/// <summary>
	/// converts flags into bytes
	/// </summary>
	/// <returns></returns>
	public override ReadOnlyMemory<byte> ToBytes()
	{
		var result = new byte[8];

		for (int i = 0; i < 8; i++)
		{
			byte b = 0;
			var baseIndex = i * 8 + 7;

			for (int j = 0; j < 8; j++)
			{
				if (flags[baseIndex])
					b |= (byte)(1 << j);

				baseIndex--;
			}

			result[i] = b;
		}

		return result;
	}
	/// <summary>
	/// creates bitmap from hexadecimal value
	/// </summary>
	/// <param name="str"></param>
	/// <returns></returns>
	public static BitmapIsoPart FromHex(string str) => FromBytes(str.FromHex());
	/// <summary>
	/// creates bitmap from byte array
	/// </summary>
	/// <param name="arr"></param>
	/// <returns></returns>
	public static BitmapIsoPart FromBytes(ReadOnlyMemory<byte> arr)
	{
		var result = new BitmapIsoPart();
		var span = arr.Span;

		for (int i = 0; i < 64; i++)
		{
			var bit = 1 << (7 - i % 8);

			result.flags[i] = (span[i / 8] & bit) == bit;
		}
		return result;
	}
	/// <summary>
	/// returns bitmap hexadecimal value
	/// </summary>
	/// <returns></returns>
	public string GetBitmapHex()
	{
		byte[] arr = new byte[8];

		for (int i = 0; i < 64; i++)
		{
			var bit = 1 << (7 - i % 8);

			if (flags[i])
				arr[i / 8] |= (byte)bit;
		}

		return arr.ToHex();
	}
	/// <summary>
	/// returns debug string showing which fields flag is set. like P03, S100
	/// </summary>
	/// <param name="fieldNo"></param>
	/// <returns></returns>
	public string ToDebugString(int fieldNo)
	{
		var prefix = (fieldNo == 0) ? "P" : "S";
		var fieldBase = (fieldNo == 0) ? 0 : 64;
		var fields = Enumerable
			.Range(0, 64)
			.Where(x => flags[x])
			.Select(x => prefix + (fieldBase + x + 1).ToString("00"));

		return $"(Bitmap) hex: {GetBitmapHex()} fields: {string.Join(", ", fields)}";
	}
	/// <summary>
	/// returns hexadecimal value of the iso bitmap
	/// </summary>
	/// <returns></returns>
	public override string ToString() => GetBitmapHex();
}
