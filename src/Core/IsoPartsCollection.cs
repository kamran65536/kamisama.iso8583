﻿namespace KamiSama.Iso8583;

/// <summary>
/// Iso Parts Collection
/// </summary>
[ExcludeFromCodeCoverage]
public class IsoPartsCollection : Dictionary<string, IsoPart?>, IDictionary<string, object?>
{
	/// <inheritdoc />
	public IsoPartsCollection()
		: base(StringComparer.InvariantCultureIgnoreCase)
	{
	}

	void IDictionary<string, object?>.Add(string key, object? value) => base.Add(key, value as IsoPart);
	bool IDictionary<string, object?>.TryGetValue(string key, out object? value)
	{
		if (base.TryGetValue(key, out var isoPart))
		{
			value = isoPart;
			return true;
		}
		else
		{
			value = null;
			return false;
		}
	}

	object? IDictionary<string, object?>.this[string key]
	{
		get => this[key];
		set => this[key] = value as IsoPart;
	}
	ICollection<string> IDictionary<string, object?>.Keys => this.Keys;
	ICollection<object?> IDictionary<string, object?>.Values => this.Values.Cast<object?>().ToList();
	void ICollection<KeyValuePair<string, object?>>.Add(KeyValuePair<string, object?> item) => base.Add(item.Key, item.Value as IsoPart);
	bool ICollection<KeyValuePair<string, object?>>.Contains(KeyValuePair<string, object?> item) 
		=> (this as IDictionary<string, IsoPart?>).Contains(new KeyValuePair<string, IsoPart?>(item.Key, item.Value as IsoPart));
	void ICollection<KeyValuePair<string, object?>>.CopyTo(KeyValuePair<string, object?>[] array, int arrayIndex)
	{
		var tmp = new KeyValuePair<string, IsoPart?>[array.Length - arrayIndex];

		(this as IDictionary<string, IsoPart?>).CopyTo(tmp, 0);

		Array.Copy(tmp, 0, array, arrayIndex, tmp.Length);
	}

	bool ICollection<KeyValuePair<string, object?>>.Remove(KeyValuePair<string, object?> item)
		=> (this as IDictionary<string, IsoPart?>).Remove(KeyValuePair.Create(item.Key, item.Value as IsoPart));
	bool ICollection<KeyValuePair<string, object?>>.IsReadOnly => (this as IDictionary<string, IsoPart?>).IsReadOnly;
	IEnumerator<KeyValuePair<string, object?>> IEnumerable<KeyValuePair<string, object?>>.GetEnumerator()
		=> (this as IDictionary<string, IsoPart?>).Select(x => KeyValuePair.Create(x.Key, (object?)x.Value)).GetEnumerator();
}