﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.BytesHandlers;

[TestClass]
public class BcdLengthValueIsoPartBytesHandlerTests
{
	[TestMethod]
	[DataRow("bcd-lvar", true)]
	[DataRow("bcd-llvar", true)]
	[DataRow("bcd-lllvar", true)]
	[DataRow("bcd-llllvar", true)]
	[DataRow("bcd-lllllvar", true)]
	[DataRow("invalid-type", false)]
	[DataRow("l", false)]
	[DataRow("var", false)]
	public void Matches_matches_type_string(string type, bool expectedResult)
		=> Assert.AreEqual(expectedResult, BcdLengthValueIsoPartBytesHandler.Matches(type));
	[TestMethod]
	[DataRow("bcd-lvar", 1)]
	[DataRow("bcd-llvar", 2)]
	[DataRow("bcd-lllvar", 3)]
	[DataRow("bcd-llllvar", 4)]
	[DataRow("bcd-lllllvar", 5)]
	public void Create_creates_BcdLengthValueIsoPartBytesHandler_with_correct_length(string type, int expectedLengthLength)
	{
		var handler = BcdLengthValueIsoPartBytesHandler.Create(type);

		Assert.IsNotNull(handler);
		Assert.AreEqual(expectedLengthLength, handler.LengthLength);
	}
	[TestMethod]
	[DataRow("invalid-type")]
	[DataRow("l")]
	[DataRow("bcd-l")]
	[DataRow("var")]
	[DataRow(null)]
	[ExpectedException(typeof(IsoProtocolParsingException))]
	public void Create_throws_IsoProtocolParsingException_if_type_is_invalid(string type)
		=> BcdLengthValueIsoPartBytesHandler.Create(type);

	[TestMethod]
	[DataRow("bcd-lvar", "020102", "0102")]
	[DataRow("bcd-lvar", "a20102", "0102")]
	[DataRow("bcd-llvar", "020102", "0102")]
	[DataRow("bcd-lllvar", "00020102", "0102")]
	[DataRow("bcd-lllvar", "a0020102", "0102")]
	[DataRow("bcd-llllvar", "00020102", "0102")]
	[DataRow("bcd-lllllvar", "0000020102", "0102")]
	[DataRow("bcd-lllllvar", "a000020102", "0102")]
	public void ReadBytes_reads_length_then_data_from_context_correctly(string type, string messageBytesHex, string expectedDataHex)
	{
		var handler = BcdLengthValueIsoPartBytesHandler.Create(type);

		var context = new IsoParsingContext(messageBytesHex.FromHex());

		var data = handler.ReadBytes(context);

		Assert.AreEqual(expectedDataHex, data.ToArray().ToHex());
	}
	[TestMethod]
	[ExpectedException(typeof(IsoMessageReadException))]
	public void ReadBytes_throws_IsoMessageReadException_when_length_is_invalid()
	{
		var messageBytesHex = "0a0102";
		var handler = BcdLengthValueIsoPartBytesHandler.Create("bcd-lvar");

		var context = new IsoParsingContext(messageBytesHex.FromHex());

		handler.ReadBytes(context);
	}
	[TestMethod]
	[DataRow("bcd-lvar", "0102", "020102")]
	[DataRow("bcd-llvar", "0102", "020102")]
	[DataRow("bcd-lllvar", "0102", "00020102")]
	[DataRow("bcd-llllvar", "0102", "00020102")]
	[DataRow("bcd-lllllvar", "0102", "0000020102")]
	public void WriteBytes_writes_length_then_data_to_the_context_correctly(string type, string dataHex, string expectedMessageBytesHex)
	{
		var handler = BcdLengthValueIsoPartBytesHandler.Create(type);

		var context = new IsoComposingContext(new IsoMessage(0200));

		handler.WrtieBytes(context, dataHex.FromHex());

		Assert.AreEqual(expectedMessageBytesHex, context.ToArray().ToHex());
	}
	[TestMethod]
	[ExpectedException(typeof(IsoMessageWriteException))]
	public void WriteBytes_throws_IsoMessageWriteException_when_data_exceeds_maximum_length_acceptable()
	{
		var dataHex = "0102030405060708090A";
		var handler = BcdLengthValueIsoPartBytesHandler.Create("bcd-lvar"); //maximum length is 9

		var context = new IsoComposingContext(new IsoMessage(0200));

		handler.WrtieBytes(context, dataHex.FromHex());
	}
}
