﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.BytesHandlers;

[TestClass]
public class FixedLengthIsoPartBytesHandlerTests
{
	[TestMethod]
	[DataRow("fixed(1)", true)]
	[DataRow("fixed(10)", true)]
	[DataRow("fixed(100)", true)]
	[DataRow("invalid-type", false)]
	[DataRow("fixed", false)]
	[DataRow("(fixed", false)]
	[DataRow("ffixed(10)", false)]
	public void Matches_matches_type_string(string type, bool expectedResult)
		=> Assert.AreEqual(expectedResult, FixedLengthIsoPartBytesHandler.Matches(type));
	[TestMethod]
	[DataRow("fixed(1)", 1)]
	[DataRow("fixed(10)", 10)]
	[DataRow("fixed(100)", 100)]
	public void Create_creates_FixedLengthIsoPartBytesHandler_with_correct_length(string type, int expectedLengthLength)
	{
		var handler = FixedLengthIsoPartBytesHandler.Create(type);

		Assert.IsNotNull(handler);
		Assert.AreEqual(expectedLengthLength, handler.Length);
	}
	[TestMethod]
	[DataRow("invalid-type")]
	[DataRow("l")]
	[DataRow("fixed")]
	[DataRow("fixed(!0)")]
	[DataRow(null)]
	[ExpectedException(typeof(IsoProtocolParsingException))]
	public void Create_throws_IsoProtocolParsingException_if_type_is_invalid(string type)
		=> FixedLengthIsoPartBytesHandler.Create(type);

	[TestMethod]
	[DataRow("fixed(1)", "01", "01")]
	[DataRow("fixed(0)", "", "")]
	[DataRow("fixed(15)", "0102030405060708090A0B0C0D0E0F", "0102030405060708090A0B0C0D0E0F")]
	public void ReadBytes_reads_length_then_data_from_context_correctly(string type, string messageBytesHex, string expectedDataHex)
	{
		var handler = FixedLengthIsoPartBytesHandler.Create(type);

		var context = new IsoParsingContext(messageBytesHex.FromHex());

		var data = handler.ReadBytes(context);

		Assert.AreEqual(expectedDataHex, data.ToArray().ToHex());
	}
	[TestMethod]
	[DataRow("fixed(1)", "01", "01")]
	[DataRow("fixed(0)", "", "")]
	[DataRow("fixed(15)", "0102030405060708090A0B0C0D0E0F", "0102030405060708090A0B0C0D0E0F")]
	public void WriteBytes_writes_length_then_data_to_the_context_correctly(string type, string dataHex, string expectedMessageBytesHex)
	{
		var handler = FixedLengthIsoPartBytesHandler.Create(type);

		var context = new IsoComposingContext(new IsoMessage(0200));

		handler.WrtieBytes(context, dataHex.FromHex());

		Assert.AreEqual(expectedMessageBytesHex, context.ToArray().ToHex());
	}
	[TestMethod]
	[ExpectedException(typeof(IsoMessageWriteException))]
	public void WriteBytes_throws_IsoMessageWriteException_when_data_exceeds_maximum_length_acceptable()
	{
		var dataHex = "010203040506070809";

		var handler = FixedLengthIsoPartBytesHandler.Create("fixed(10)"); //expecting 10 bytes but receiving 9

		var context = new IsoComposingContext(new IsoMessage(0200));

		handler.WrtieBytes(context, dataHex.FromHex());
	}
}
