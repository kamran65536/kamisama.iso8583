﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.TypeHandlers;

[TestClass]
public class BitmapIsoPartTypeHandlerTests
{
	[TestMethod]
	[DataRow("bmp", true)]
	[DataRow("bitmap", true)]
	[DataRow("binary-bitmap", true)]
	[DataRow("", false)]
	public void Matches_matches_type_with_correct_values(string type, bool expectedResult)
	{
		var result = BitmapIsoPartTypeHandler.Matches(type);

		Assert.AreEqual(expectedResult, result);
	}
	[TestMethod]
	public void Create_returns_new_BitmapIsoPartTypeHandler()
	{
		var handler = BitmapIsoPartTypeHandler.Create("bitmap");

		Assert.IsNotNull(handler);
	}
	[TestMethod]
	[DataRow("E000000000004008")]
	[DataRow("0000000002400001")]
	public void ConvertToIsoPart_returns_BitmapIsoPart_for_data(string hexData)
	{
		var arr = hexData.FromHex();
		var handler = BitmapIsoPartTypeHandler.Create("bitmap");

		var part = handler.ConvertToIsoPart(arr);

		var BitmapIsoPart = part as BitmapIsoPart;

		Assert.IsNotNull(BitmapIsoPart);
		Assert.AreEqual(hexData, BitmapIsoPart.GetBitmapHex());
	}
	[TestMethod]
	[ExpectedException(typeof(NotSupportedException))]
	public void ConvertToBytes_throws_NotSupportedException_if_part_is_not_BitmapIsoPart()
	{
		var handler = BitmapIsoPartTypeHandler.Create("bitmap");
		var part = new TextIsoPart("1234");

		handler.ConvertToBytes(part);
	}
	[TestMethod]
	[ExpectedException(typeof(NullReferenceException))]
	public void ConvertToBytes_throws_NullReferenceException_if_part_is_null()
	{
		var handler = BitmapIsoPartTypeHandler.Create("bitmap");
		handler.ConvertToBytes(null);
	}
	[TestMethod]
	[DataRow("E000000000004008")]
	[DataRow("0000000002400001")]
	public void CovertToBytes_returns_corresponding_TextIsoPart_Data(string dataHex)
	{
		var handler = BitmapIsoPartTypeHandler.Create("bitmap");
		var part = BitmapIsoPart.FromHex(dataHex);

		var bytes = handler.ConvertToBytes(part);

		Assert.AreEqual(dataHex, bytes.ToArray().ToHex());
	}
}
