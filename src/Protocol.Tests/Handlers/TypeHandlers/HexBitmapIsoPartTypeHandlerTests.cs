﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.TypeHandlers;

[TestClass]
public class HexBitmapIsoPartTypeHandlerTests
{
	[TestMethod]
	[DataRow("hex-bmp", true)]
	[DataRow("hex-bitmap", true)]
	[DataRow("bmp-hex", true)]
	[DataRow("bitmap-hex", true)]
	[DataRow("", false)]
	public void Matches_matches_type_with_correct_values(string type, bool expectedResult)
	{
		var result = HexBitmapIsoPartTypeHandler.Matches(type);

		Assert.AreEqual(expectedResult, result);
	}
	[TestMethod]
	public void Create_returns_new_HexBitmapIsoPartTypeHandler()
	{
		var handler = HexBitmapIsoPartTypeHandler.Create("hex-bitmap");

		Assert.IsNotNull(handler);
	}
	[TestMethod]
	[DataRow("45303030303030303030303034303038")]
	[DataRow("30303030303030303032343030303031")]
	public void ConvertToIsoPart_returns_HexBitmapIsoPart_for_data(string hexData)
	{
		var arr = hexData.FromHex();
		var handler = HexBitmapIsoPartTypeHandler.Create("hex-bitmap");

		var part = handler.ConvertToIsoPart(arr);

		var BitmapIsoPart = part as BitmapIsoPart;

		Assert.IsNotNull(BitmapIsoPart);
		Assert.AreEqual(hexData.FromHex().GetString(), BitmapIsoPart.GetBitmapHex());
	}
	[TestMethod]
	[ExpectedException(typeof(NotSupportedException))]
	public void ConvertToBytes_throws_NotSupportedException_if_part_is_not_HexBitmapIsoPart()
	{
		var handler = HexBitmapIsoPartTypeHandler.Create("hex-bitmap");
		var part = new TextIsoPart("1234");

		handler.ConvertToBytes(part);
	}
	[TestMethod]
	[ExpectedException(typeof(NullReferenceException))]
	public void ConvertToBytes_throws_NullReferenceException_if_part_is_null()
	{
		var handler = HexBitmapIsoPartTypeHandler.Create("hex-bitmap");
		handler.ConvertToBytes(null);
	}
	[TestMethod]
	[DataRow("45303030303030303030303034303038")]
	[DataRow("30303030303030303032343030303031")]
	public void CovertToBytes_returns_corresponding_BitmapIsoPart_Data(string hexHexData)
	{
		var hexData = hexHexData.FromHex().GetString();
		var handler = HexBitmapIsoPartTypeHandler.Create("hex-bitmap");
		var part = BitmapIsoPart.FromHex(hexData);

		var bytes = handler.ConvertToBytes(part);

		Assert.AreEqual(hexHexData, bytes.ToArray().ToHex());
	}
}
