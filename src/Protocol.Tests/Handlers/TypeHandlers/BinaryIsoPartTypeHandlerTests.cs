﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.TypeHandlers;

[TestClass]
public class BinaryIsoPartTypeHandlerTests
{
	[TestMethod]
	[DataRow("bin", true)]
	[DataRow("b", true)]
	[DataRow("binary", true)]
	[DataRow("", false)]
	public void Matches_matches_type_with_correct_values(string type, bool expectedResult)
	{
		var result = BinaryIsoPartTypeHandler.Matches(type);

		Assert.AreEqual(expectedResult, result);
	}
	[TestMethod]
	public void Create_returns_new_BinaryIsoPartTypeHandler()
	{
		var handler = BinaryIsoPartTypeHandler.Create("binary");

		Assert.IsNotNull(handler);
	}
	[TestMethod]
	[DataRow("123456")]
	[DataRow("23456")]
	public void ConvertToIsoPart_returns_BinaryIsoPart_for_data(string hexData)
	{
		var arr = hexData.FromHex();
		var handler = BinaryIsoPartTypeHandler.Create("binary");

		var part = handler.ConvertToIsoPart(arr);

		var binaryIsoPart = part as BinaryIsoPart;

		Assert.IsNotNull(binaryIsoPart);
		Assert.AreEqual(arr.ToHex(), binaryIsoPart.Data.ToArray().ToHex());
	}
	[TestMethod]
	[ExpectedException(typeof(NotSupportedException))]
	public void ConvertToBytes_throws_NotSupportedException_if_part_is_not_BinaryIsoPart()
	{
		var handler = BinaryIsoPartTypeHandler.Create("binary");
		var part = new TextIsoPart("1234");

		handler.ConvertToBytes(part);
	}
	[TestMethod]
	[ExpectedException(typeof(NullReferenceException))]
	public void ConvertToBytes_throws_NullReferenceException_if_part_is_null()
	{
		var handler = BinaryIsoPartTypeHandler.Create("binary");
		handler.ConvertToBytes(null);
	}
	[TestMethod]
	[DataRow("123456")]
	[DataRow("23456")]
	public void CovertToBytes_returns_corresponding_TextIsoPart_Data(string data)
	{
		var arr = data.FromHex();
		var handler = BinaryIsoPartTypeHandler.Create("binary");
		var part = new BinaryIsoPart(arr);

		var bytes = handler.ConvertToBytes(part);

		Assert.AreEqual(arr.ToHex(), bytes.ToArray().ToHex());
	}
}
