﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KamiSama.Iso8583.Tests;

[TestClass]
public class IsoMessageTests
{
	[TestMethod]
	public void Id_sets_and_gets_correctly()
	{
		var message = new IsoMessage(1100);

		Assert.AreEqual(1100, message.Id);
	}
	[TestMethod]
	public void Indexer_returns_part_item_by_name_when_found()
	{
		var expectedProcessCode = "123456";
		var message = new IsoMessage();

		message["process-code"] = expectedProcessCode;

		Assert.AreEqual(expectedProcessCode, message["process-code"] + "");
		Assert.AreEqual(expectedProcessCode, message["Process-code"] + "");
		Assert.AreEqual(expectedProcessCode, message["Process-Code"] + "");
	}
	[TestMethod]
	public void Indexer_returns_null_when_item_is_not_found()
	{
		var message = new IsoMessage();

		Assert.IsNull(message["not-found"]);
	}
	[TestMethod]
	public void Indexer_returns_field_by_fieldNo_when_found()
	{
		var expectedProcessCode = "123456";
		var message = new IsoMessage();

		message[3] = expectedProcessCode;

		Assert.AreEqual(expectedProcessCode, message[3] + "");
		Assert.AreEqual(expectedProcessCode, message["p03"] + "");
		Assert.AreEqual(expectedProcessCode, message["P03"] + "");
	}
	[TestMethod]
	public void Indexer_sets_PrimaryBitmap_for_fieldNo_zero()
	{
		var bitmap = new BitmapIsoPart();

		var message = new IsoMessage();

		message[0] = bitmap;

		Assert.AreEqual(bitmap, message.PrimaryBitmap);
		Assert.AreEqual(bitmap, message["p00"]);
		Assert.AreEqual(bitmap, message["P00"]);
	}
	[TestMethod]
	public void Indexer_sets_SecondaryBitmap_for_fieldNo_one()
	{
		var bitmap = new BitmapIsoPart();

		var message = new IsoMessage();

		message[1] = bitmap;

		Assert.AreEqual(bitmap, message.SecondaryBitmap);
		Assert.AreEqual(bitmap, message["p01"]);
		Assert.AreEqual(bitmap, message["P01"]);
	}
	[TestMethod]
	public void Indexer_returns_null_by_fieldNo_when_not_found()
	{
		var message = new IsoMessage();

		Assert.IsNull(message[3]);
		Assert.IsNull(message["p03"]);
	}
	[TestMethod]
	public void Indexer_sets_secondaryBitmap_when_fields_above_64_are_set()
	{
		var expectedField = "123456";
		var message = new IsoMessage();

		Assert.IsNotNull(message.PrimaryBitmap);
		Assert.IsNull(message.SecondaryBitmap);

		message[65] = expectedField;

		Assert.IsNotNull(message.SecondaryBitmap);
		Assert.IsNotNull(message[1]);
		Assert.IsNotNull(message["p01"]); //secondary bitmap
		Assert.AreEqual(expectedField, message[65] + "");
		Assert.AreEqual(expectedField, message["s065"] + "");
		Assert.AreEqual(expectedField, message["S065"] + "");
	}
	[TestMethod]
	public void ToDebugStringByField_returns_fields_and_their_values()
	{
		var pan = "62802311000001";
		var processCode = "500000";
		var pinBlock = "E5E3A466435";

		var message = new IsoMessage(200);

		message[2] = pan;
		message[3] = processCode;
		message[52] = pinBlock;

		var str = message.ToDebugStringByFields(52);

		Assert.IsTrue(str.Contains("0200")); //MTI (message type indicator)
		Assert.IsTrue(str.Contains(pan));
		Assert.IsTrue(str.Contains(processCode));
		Assert.IsFalse(str.Contains(pinBlock));
		Assert.IsTrue(str.Contains("........"));//hidden pin block
	}
	[TestMethod]
	public void ToDebugStringByParts_returns_fields_and_their_values()
	{
		var pan = "62802311000001";
		var processCode = "500000";
		var pinBlock = "E5E3A466435";

		var message = new IsoMessage(200);

		message["pan"] = pan;
		message["process-code"] = processCode;
		message["pin-block"] = pinBlock;

		var str = message.ToDebugStringByParts("pin-block");

		Assert.IsTrue(str.Contains("0200")); //MTI (message type indicator)
		Assert.IsTrue(str.Contains(pan));
		Assert.IsTrue(str.Contains(processCode));
		Assert.IsFalse(str.Contains(pinBlock));
		Assert.IsTrue(str.Contains("........"));//hidden pin block
	}
	[TestMethod]
	public void ToString_returns_ToDebugStringByParts()
	{
		var pan = "62802311000001";
		var processCode = "500000";
		var pinBlock = "E5E3A466435";

		var message = new IsoMessage(200);

		message["pan"] = pan;
		message["process-code"] = processCode;
		message["pin-block"] = pinBlock;

		var str = message.ToDebugStringByParts("pin-block");

		Assert.AreEqual(str, message.ToString());
	}
	[TestMethod]
	[DataRow(3, true, true)]
	[DataRow(3, false, true)]
	[DataRow(65, true, true)]
	[DataRow(65, false, false)]
	public void Has_checks_bitmaps_for_fields(int fieldNo, bool secondaryBitmapIsSet, bool expectedResult)
	{
		var message = new IsoMessage(1100);

		message.PrimaryBitmap[3] = true;

		if (secondaryBitmapIsSet)
		{
			message[1] = new BitmapIsoPart();
			message.SecondaryBitmap[65] = true;
		}
		
		Assert.AreEqual(expectedResult, message.Has(fieldNo));
	}
	[TestMethod]
	public void GetMac_returns_P64_for_mac_if_SecondaryBitmap_is_not_present()
	{
		var mac = new BinaryIsoPart(new byte[] { 0x01, 0x02, 0x03, 0x04 });
		var message = new IsoMessage(1200);

		message[64] = mac;

		Assert.AreSame(mac, message.GetMac());
	}
	[TestMethod]
	public void GetMac_returns_S128_for_mac_if_SecondaryBitmap_is_present()
	{
		var mac = new BinaryIsoPart(new byte[] { 0x01, 0x02, 0x03, 0x04 });
		var message = new IsoMessage(1200);
		message[1] = new BitmapIsoPart();

		message[128] = mac;

		Assert.AreSame(mac, message.GetMac());
	}
	[TestMethod]
	[DataRow(2, 64)]
	[DataRow(64, 64)]
	[DataRow(65, 128)]
	[DataRow(128, 128)]
	public void SetMacFlag_should_set_mac_flag_correctly(int fieldNo, int expectedMacFieldNo)
	{
		var message = new IsoMessage(1200);

		message[fieldNo] = "123456";

		message.SetMacFlag();

		Assert.IsTrue(message.Has(expectedMacFieldNo));
	}
}
