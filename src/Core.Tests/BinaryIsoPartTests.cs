﻿using KamiSama.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace KamiSama.Iso8583.Tests;

[TestClass]
public class BinaryIsoPartTests
{
	[TestMethod]
	public void Constrcutor_must_set_Data()
	{
		var data = new byte[] { 0x01, 0x02, 0x03, 0x04 };

		var part = new BinaryIsoPart(data);

		CollectionAssert.AreEqual(part.Data.ToArray(), data);
	}
	[TestMethod]
	public void Constructor_without_parameter_sets_Empty_ReadOnlyMemory()
	{
		var part = new BinaryIsoPart();

		Assert.AreEqual(ReadOnlyMemory<byte>.Empty, part.Data);
	}
	[TestMethod]
	public void Implicit_cast_must_create_BinaryIsoPart_from_byte_array()
	{
		var data = new byte[] { 0x01, 0x02, 0x03, 0x04 };
		var part = (BinaryIsoPart) data;

		CollectionAssert.AreEqual(part.Data.ToArray(), data);
	}
	[TestMethod]
	public void Implicit_cast_must_create_BinaryIsoPart_from_ReadOnlyMemory()
	{
		var data = new byte[] { 0x01, 0x02, 0x03, 0x04 };
		var part = (BinaryIsoPart)new ReadOnlyMemory<byte>(data);

		CollectionAssert.AreEqual(part.Data.ToArray(), data);
	}
	[TestMethod]
	public void ToString_returns_hexadecimal_value_of_Data()
	{
		var data = new byte[] { 0x01, 0x02, 0x03, 0x04 };
		var part = new BinaryIsoPart(data);

		Assert.AreEqual(data.ToHex(), part.ToString());
	}
	[TestMethod]
	public void ToBytes_returns_Data()
	{
		var data = new byte[] { 0x01, 0x02, 0x03, 0x04 };
		var part = new BinaryIsoPart(data);

		CollectionAssert.AreEqual(data.ToArray(), part.ToBytes().ToArray());
	}
}
