﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Mac;
using KamiSama.Iso8583.Protocol.Parts;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol;

/// <summary>
/// Iso Message Definition which can be parsed.
/// </summary>
[XmlRoot("message")]
public class IsoMessageDefinition : IsoMessageDefinitionBase, IEquatable<IsoMessageDefinition>
{
	/// <summary>
	/// all fields even they are null avoiding dictionary search
	/// </summary>
	internal IsoFieldDefinition?[]? optimizedFields;
	/// <inheritdoc/>
	public override void Optimize(IsoProtocol protocol, bool forceOptimize = false)
	{
		if ((optimizedFields != null) && !forceOptimize)
			return;

		base.Optimize(protocol, forceOptimize);

		optimizedFields = Enumerable
			.Range(0, 129)
			.Select(fieldNo => Fields.FirstOrDefault(x => x.FieldNo == fieldNo))
			.ToArray();
	}
	/// <summary>
	/// parses the message based on definition
	/// </summary>
	/// <param name="context"></param>
	/// <param name="macGenerator"></param>
	public virtual async Task ParseAsync(IsoParsingContext context, IMacGenerator macGenerator)
	{
		for (int fieldNo = 0; fieldNo <= 128; fieldNo++)
			try
			{
				if ((fieldNo != 0) && !context.Message!.Has(fieldNo))
					continue;

				var field = optimizedFields![fieldNo] ?? throw new IsoMessageReadException($"Cannot find iso field definition for {fieldNo}.");

				await field.ParseAsync(context, macGenerator);
			}
			catch (Exception exp) when (exp is not MacVerificationFailedException)
			{
				string fieldName = fieldNo switch
				{
					0 => "P00 (Primary bitmap)",
					1 => "P01 (Secondary Bitmap)",
					< 65 => $"P{fieldNo:00}",
					_ => $"S{fieldNo:000}"
				};
				
				throw new IsoMessageReadException($"Error in field: {fieldName}", exp);
			}
	}
	/// <summary>
	/// Composes message into the context
	/// </summary>
	/// <param name="context"></param>
	/// <param name="macGenerator"></param>
	public virtual async Task ComposeAsync(IsoComposingContext context, IMacGenerator macGenerator)
	{
		for (int fieldNo = 0; fieldNo <= 128; fieldNo++)
			try
			{
				if ((fieldNo != 0) && !context.Message!.Has(fieldNo))
					continue;

				var field = optimizedFields![fieldNo] ?? throw new IsoMessageWriteException($"Cannot find iso field definition for {fieldNo}.");

				await field.ComposeAsync(context, macGenerator);
			}
			catch (Exception exp) when (exp is not MacGenerationFailedException)
			{
				string fieldName = fieldNo switch
				{
					0 => "P00 (Primary bitmap)",
					1 => "P01 (Secondary Bitmap)",
					< 65 => $"P{fieldNo:00}",
					_ => $"S{fieldNo:000}"
				};

				throw new IsoMessageWriteException($"Error in field: {fieldName}", exp);
			}
	}
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public bool Equals(IsoMessageDefinition? other)
	{
		if ((other == null) ||
			(this.Name != other.Name) ||
			(this.BaseTemplateName != other.BaseTemplateName) ||
			(this.Fields.Length != other.Fields.Length))
			return false;

		for (int i = 0; i < Fields.Length; i++)
			if (!Fields[i].Equals(other.Fields[i]))
				return false;

		return true;
	}
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public override bool Equals(object? obj) => Equals(obj as IsoMessageDefinition);
	/// <inheritdoc />
	[ExcludeFromCodeCoverage]
	public override int GetHashCode() => base.GetHashCode();
}