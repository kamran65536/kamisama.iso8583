﻿using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;

namespace KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;

/// <summary>
/// Factory to add (customized) <see cref="IIsoPartTypeHandler"/> to <see cref="IIsoPartBytesHandler.HandlerFactories"/>
/// </summary>
public interface IIsoPartBytesHandlerFactory
{
	/// <summary>
	/// Returns true if the <paramref name="type"/> matches the factory handler type.
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	bool Matches(string type);
	/// <summary>
	/// Creates new type based on the type
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	IIsoPartBytesHandler Create(string type);
}
