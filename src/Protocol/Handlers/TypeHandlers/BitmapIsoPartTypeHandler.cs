﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;

/// <summary>
/// Converts <see cref="BitmapIsoPart"/> into bytes and vice versa
/// </summary>
public class BitmapIsoPartTypeHandler : IIsoPartTypeHandler
{
	/// <summary>
	/// Matches type to bmp, bitmap, binary-bitmap
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	[ExcludeFromCodeCoverage]
	public static bool Matches(string type)
	{
		return type switch
		{
			"bmp" or "bitmap" or "binary-bitmap" => true,
			_ => false,
		};
	}
	/// <summary>
	/// Creates new <see cref="BitmapIsoPartTypeHandler"/>
	/// </summary>
	/// <param name="type"></param>
	[SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
	public static BitmapIsoPartTypeHandler Create(string type) => new();
	/// <summary>
	/// Converts bytes into <see cref="BitmapIsoPart"/>
	/// </summary>
	/// <param name="bytes"></param>
	/// <returns></returns>
	public IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes) => BitmapIsoPart.FromBytes(bytes);
	/// <summary>
	/// Converts <see cref="BitmapIsoPart"/> into bytes
	/// </summary>
	/// <exception cref="NotSupportedException">for any other IsoPart type</exception>
	/// <exception cref="NullReferenceException">if part is null</exception>
	/// <param name="part"></param>
	/// <returns></returns>
	[ExcludeFromCodeCoverage]
	public ReadOnlyMemory<byte> ConvertToBytes(IsoPart part) =>
		part switch
		{
			BitmapIsoPart bitmapIsoPart => bitmapIsoPart.ToBytes(),
			null => throw new NullReferenceException($"part cannot be null."),
			_ => throw new NotSupportedException($"{part.GetType()} is not supported by {nameof(BcdIsoPartTypeHandler)}"),
		};
}