﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;

/// <summary>
/// Converts Bcd string into bytes and vice versa
/// </summary>
public class BcdIsoPartTypeHandler : IIsoPartTypeHandler
{
	/// <summary>
	/// Creates new <see cref="BcdIsoPartTypeHandler"/>
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	[SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
	public static BcdIsoPartTypeHandler Create(string type) => new();
	/// <summary>
	/// Matches the type for bcd
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	public static bool Matches(string type) => type == "bcd";
	/// <summary>
	/// Converts bytes to <see cref="TextIsoPart"/>
	/// </summary>
	/// <param name="bytes"></param>
	/// <returns></returns>
	public IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes) => new TextIsoPart(MemoryBytesHelper.ConvertBcdBytesToString(bytes));
	/// <summary>
	/// Converts <see cref="TextIsoPart"/> into bytes
	/// </summary>
	/// <exception cref="NotSupportedException">for any other IsoPart type</exception>
	/// <exception cref="NullReferenceException">if part is null</exception>
	/// <param name="part"></param>
	/// <returns></returns>
	public ReadOnlyMemory<byte> ConvertToBytes(IsoPart part)
	{
		if (part == null)
			throw new NullReferenceException("part cannot be null");
		else if (part is TextIsoPart textIsoPart)
			try
			{
				return MemoryBytesHelper.ConvertNumberStringToBcdBytes(textIsoPart.Data);
			}
			catch (Exception exp)
			{
				throw new IsoMessageWriteException($"Cannot convert {textIsoPart.Data} to bcd string.", innerException: exp);
			}
		else
			throw new NotSupportedException($"{part.GetType()} is not supported by {nameof(BcdIsoPartTypeHandler)}");
	}
}