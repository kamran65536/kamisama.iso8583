﻿using KamiSama.Iso8583.Protocol.Exceptions;

namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;

/// <summary>
/// Default <see cref="IIsoPartTypeHandlerFactory"/>
/// </summary>
/// <remarks>this class will automatically detects Matches and Create method to create the handler</remarks>
/// <typeparam name="THandler"></typeparam>
public class DefaultIsoPartTypeHandlerFactory<THandler> : IIsoPartTypeHandlerFactory
	where THandler : IIsoPartTypeHandler
{
	/// <summary>
	/// matches the string to the corresponding <see cref="IIsoPartTypeHandler"/>
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	public bool Matches(string type)
	{
		var matchesMethod = typeof(THandler).GetMethod("Matches") ?? throw new IsoProtocolParsingException($"Cannot find Matches method in type {typeof(THandler)}.");
		return (bool)matchesMethod.Invoke(null, [type])!;
	}
	/// <summary>
	/// Creates a new instance of bytes handler
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	public IIsoPartTypeHandler Create(string type)
	{
		var createMethod = typeof(THandler).GetMethod("Create") ?? throw new IsoProtocolParsingException($"Cannot find Matches method in type {typeof(THandler)}.");
		return (IIsoPartTypeHandler)createMethod.Invoke(null, [type])!;
	}
}
