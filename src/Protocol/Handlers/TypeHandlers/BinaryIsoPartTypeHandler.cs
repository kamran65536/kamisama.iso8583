﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;

/// <summary>
/// Converts <see cref="BinaryIsoPart"/> into bytes and vice versa
/// </summary>
public class BinaryIsoPartTypeHandler : IIsoPartTypeHandler
{
	/// <summary>
	/// Matches type to bin, b, binary
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	[ExcludeFromCodeCoverage]
	public static bool Matches(string type)
	{
		return type switch
		{
			"bin" or "b" or "binary" => true,
			_ => false,
		};
	}


	/// <summary>
	/// Creates new <see cref="BinaryIsoPartTypeHandler"/>
	/// </summary>
	/// <param name="type"></param>
	/// <returns></returns>
	[SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
	public static BinaryIsoPartTypeHandler Create(string type) => new();
	/// <summary>
	/// Converts bytes into <see cref="BinaryIsoPart"/>
	/// </summary>
	/// <param name="bytes"></param>
	/// <returns></returns>
	public IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes) => new BinaryIsoPart(bytes);
	/// <summary>
	/// Converts <see cref="BinaryIsoPart"/> into bytes
	/// </summary>
	/// <exception cref="NotSupportedException">for any other IsoPart type</exception>
	/// <exception cref="NullReferenceException">if part is null</exception>
	/// <param name="part"></param>
	/// <returns></returns>
	[ExcludeFromCodeCoverage]
	public ReadOnlyMemory<byte> ConvertToBytes(IsoPart part) =>
		part switch
		{
			BinaryIsoPart binaryIsoPart => binaryIsoPart.Data,
			null => throw new NullReferenceException("part cannot be null."),
			_ => throw new NotSupportedException($"{part.GetType()} is not supported by {nameof(BinaryIsoPartTypeHandler)}"),
		};
}