﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions;

/// <summary>
/// Cannot put message bytes on stream, see message
/// </summary>
/// <remarks>
/// constructor
/// </remarks>
/// <param name="message"></param>
/// <param name="innerException"></param>
[ExcludeFromCodeCoverage]
[Serializable]
public class CannotPutMessageBytesOnStreamException(string message = "Cannot put message byes on stream.", Exception? innerException = null) 
	: IsoMessageWriteException(message, innerException)
{
}
