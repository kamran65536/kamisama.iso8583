﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions;

/// <summary>
/// MacVerificationFailedException; this exception will be thrown if mac verification is failed in reading message
/// </summary>
/// <remarks>
/// constructor
/// </remarks>
/// <param name="message"></param>
/// <param name="innerException"></param>
[Serializable]
[ExcludeFromCodeCoverage]
public class MacVerificationFailedException(string message, Exception? innerException = null) 
	: IsoMessageReadException(message, innerException)
{
	/// <summary>
	/// constructor
	/// </summary>
	/// <param name="messageMacHex"></param>
	/// <param name="generatedMacHex"></param>
	public MacVerificationFailedException(string messageMacHex, string generatedMacHex)
		: this($"Mac verification fiald, Message MAC: {messageMacHex}, Generated MAC: {generatedMacHex}")
	{
	}
}