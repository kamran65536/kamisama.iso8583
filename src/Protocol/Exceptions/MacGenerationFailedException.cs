﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions;

/// <summary>
/// Thrown when mac generation failed see the message
/// </summary>
/// <remarks>
/// constructor
/// </remarks>
/// <param name="message"></param>
/// <param name="innerException"></param>
[ExcludeFromCodeCoverage]
[Serializable]
public class MacGenerationFailedException(string message = "MAC generation failed.", Exception? innerException = null)
	: IsoMessageWriteException(message, innerException)
{
}
