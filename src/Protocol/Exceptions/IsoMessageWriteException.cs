﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions;

/// <summary>
/// IsoMessageWriteException; this exception will be thrown in different iso message composing stages
/// </summary>
/// <remarks>
/// constructor
/// </remarks>
/// <param name="message"></param>
/// <param name="innerException"></param>
[Serializable]
[ExcludeFromCodeCoverage]
public class IsoMessageWriteException(string message, Exception? innerException = null)
	: Exception(message, innerException)
{
}
