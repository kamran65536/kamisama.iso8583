## 3. Powerful Protocol
KamiSama.Iso8583.Protocol library provides fast and flexible IsoProtocol library. The library can parse and compose iso messages from/to stream.
Here are some features:
- support for various data types, such as:
    - text
    - binary
    - hex-binary
    - bcd
    - bitmap
- support for various length formats, such as:
    - fixed(n)
    - lvar, llvar, lllvar, llllvar, lllllvar
    - bcd-lvar, bcd-llvar, bcd-lllvar, bcd-llllvar, bcd-lllllvar
    - bin-lvar, bin-llvar, bin-lllvar, bin-llllvar, bin-lllllvar
- optimized for performance "as fast as possible" and memory management; "as lightweight as possible"
- support for different message protocol for different message types
- template for different messages
    ```csharp
    var protocol = new IsoProtocol 
    {
        MessageTemplateDefnitions = new []
        {
            new IsoMessageTemplateDefinition
            {
                Name = "T1",
                Fields = new []
                {
                    new IsoFieldDefinition { FieldNo = 0, Type = "fixed(16) hex-bmp" }
                }
            }
        }
        MessageDefinitions = new []
        {
            new IsoMessageDefinition
            {
                Name = "Default",
                BaseTemplateName = "T1",
                Fields = new []
                {
                    new IsoFieldDefinition { FieldNo = 48, Type = "llvar text" }
                }
            }
        }
    };
    ```
### 3.1. Building Protocol
You can either build protocol from different ways:
- create protocol in code
- load protocol from a single xml file
- load protocol from a directory
- load protocol from a Zip file or archive
- load from old version protocol file (v1)

#### 3.1.1 Coded protocol
You can build protocol right in your code:
```csharp
var protocol = new IsoProtocol 
{
    MessageDefinitions = new []
    {
        new IsoMessageDefinition
        {
            Name = "Default",
            Fields = new []
            {
                new IsoFieldDefinition { FieldNo = 0, Type = "fixed(16) hex-bmp" }
            }
        }
    }
};

protocol.Optimize();                    // you should allways call this method if you are building the protocol manually
````
#### 3.1.2. Load from a Single XML
```csharp
    var protocol = IsoProtocol.LoadFromXml("./protocols/single.protocol.xml");

    //there you go!
```
#### 3.1.3 Load from a Directory
If the directory contains a file named: "___definition.protocol.xml___" the method will automatically use it as the base protocol definition file (eg. header information)

```csharp
    var protocol = IsoProtocol.LoadFromDirectory("./protocols/sample-directory/");

    //there you go!
```
#### 3.1.4 Load from a Zip File
You can either load your protocol from a zip file path or ZipArchive directory.

```csharp
    var protocol = IsoProtocol.LoadFromZipFile("./protocols/zipped-protocol.zip");
    var protocol2 = IsoProtocol.LoadFromZipFile(zipArchive);
    //there you go!
```
#### 3.1.5 Load from an GenericIsoProtocol v1
The library still supports the old iso protocol file format for backward compatibility, if you still want to use it. But it is not recommended as the support may be dropped in the next versions.

```csharp
    var protocol = GenericIsoProtocol.LoadFromFile("./protocols/old.protocol.xml");
    
    //there you go!
```
### 3.2. Deep into the protocol
The protocol consist of different message templates and message definitions. The message templates and message definitions structure are basically the same and includes field definitions.

### 3.2.1. Message Templates
The purpose of defining message template is to specify a template structore for group of messages. Message templates can be also hierarchical.
here is a sample message template:
```xml
<?xml version="1.0" encoding="utf-8"?>
<template name="financial" matches="[mti] == &quot;0200&quot;">
	<field no="0" type="fixed(16) hex-bmp" />
	<field no="1" type="fixed(16) hex-bmp" />
	<field no="2" type="llvar text" map-to="pan" />
	<field no="3" type="fixed(6) text" map-to="process-code" />
</template>
```

As you see the template contains a match criteria to handle only financial messages (MTI = 0200).
### 3.2.2 Message Definitions
Message definition format are almost the same as template ones. Message definitions can refer to message template; it means the message structure will be inherited from message template and new ones will be _replaced_.
here is a sample message definition:
```xml
<?xml version="1.0" encoding="utf-8"?>
<message name="purchase" base="financial" matches="[p03] == &quot;000000&quot;">
	<field no="48" type="lllvar binary" map-to="additional-data">
		<part type="any" condition="[p24] == &quot;205&quot;" map-to="custom-data" />
	</field>
</message>
```

The message definition is for purchase messsages (process code = 000000) and will automatically inherit financial message template.
### 3.2.3 Field Definitions
The field definition is simple and straight forward. It contains field number (a number between 0 to 128) and a type definition. The type definition is a text based definition consists of mainly two to three parts.

A field also can be parsed into multiple iso part.

#### 3.2.3.1 Size Definition
The first part will indicate how the field should be read from message bytes. Here are some examples and their meanings;

Size Definition         |   Meanding
------------------------|----------------------------------------------------------------
fixed(n)                |   fixed n bytes to read from stream
ascii-lvar, llvar...    |   first read m bytes for length (m = number of 'l') as ascii text number and then read bytes according to the length. upto 5 l is supported.
bin-lvar, bin-llvar,... |   first read m bytes for length (m = number of 'l') as binary number and then read bytes according to the length. upto 5 l is supported.
bcd-lvar, bcd-llvar,... |   first read m nibbles (m/2 bytes) for length (m = number of 'l') as bcd number and then read bytes according to the length. upto 5 l is supported.
any                     |   no length, read/write any possible bytes.

#### 3.2.3.2 Type Definition
The remained section of type definition is processed as _type definition_. The IsoMessage basically supports three types of field: binary, text, bitmap.
Here are some examples and their meanings:

Size Definition         |   Meanding
------------------------|----------------------------------------------------------------
bin, binary, b          |   binary value
hex-bin, hex-binary, hb |   hexdecimal text value to be converted to binary
bcd                     |   bcd value assumed as number and converted to text
bmp, bitmap, bin-bmp    |   binary bitmap value converted to iso bitmap (primary and secondary)
hex-bmp, hex-bitmap     |   hexadecimal text converted to bitmap
text, text utf8         |   text value with encoding; encoding is optional and default value is UTF-8.
mac, mac ___mac-type___ |   mac field, converted to binary. mac type is optional and default value is ansix9.19.
hex-mac, hex-mac ___mac-type___ | hexadecimal mac field, convert to binary. mac type is optional and default value is ansix9.19.

Possible values for mac type are:
- skip: do not generate mac for message, use the value given by user.
- ansi9.19,ansix9.19,x9.19  : use ANSI X9.19 mac algorithm.
- des: use DES MAC algorithm.
- 3des, tirpledes : use 3DES MAC algorithm.
- aes: use AES MAC algorithm.

### 3.2.4 Iso Parts
A field also can be parsed into multiple iso part. The iso part structure is almost the same as iso fields. There are some differences between iso part and iso field:
- iso field has field number but iso part does not.
- iso part has condition for processing but iso field does not.

iso part also can be hierarchical and includes sub-parts too.

here is a sample iso part and iso field:
```xml
<field no="48" type="lllvar binary" map-to="additional-data">
		<part type="any" condition="[p24] == &quot;205&quot;" map-to="custom-data" />
</field>
```