﻿using KamiSama.Extensions;
using KamiSama.Iso8583;
using KamiSama.Iso8583.Client;
using KamiSama.Iso8583.Protocol;
using KamiSama.Iso8583.Protocol.Generic;
using KamiSama.Iso8583.Protocol.Mac;

namespace Client;

internal class Program
{
	static async Task Main(string[] args)
	{
		Console.WriteLine("Press enter to send message.");
		Console.ReadKey();

		var protocol = IsoProtocol.LoadFromXmlFile("./single.protocol.xml");
		var pinKey = "1122334455667788".FromHex();
		var macKey = "1122334455667788".FromHex();

		var macGenerator = IMacGenerator.FromKey(macKey);

		var client = new Iso8583Client(protocol);

		var request = new IsoMessage(200)
		{
			[2] = "62802300000000000", //pan
			[3] = "000000", //process code
			[11] = "123456", // stan
			[52] = "88AE2BFAAF5CC583" //pin block
		};


		await client.SendAsync(request, "localhost", 12000, macGenerator, CancellationToken.None, TimeSpan.Zero);
	}
}
